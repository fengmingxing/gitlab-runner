FROM cr-demo-cn-beijing.cr.volces.com/tools/java:8
COPY spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar /opt/target/spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar
EXPOSE 8080
CMD java -jar /opt/target/spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar
# add your tools here ...
